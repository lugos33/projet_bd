import MySQLdb
import MySQLdb.cursors
import hashlib

# Valide les infos de l'utilisateur et le connecte ou non
def info_user(username, password):
    user_actif = {'UserName':'','Profil':''}
    db = MySQLdb.connect("localhost", "Equipe17", "SecurePassword", "projet_equipe17")
    cursor = db.cursor()
    cursor.execute("select username from utilisateur where username = '" + username + "' and password = '" +
                   hashlib.sha3_512(password.encode()).hexdigest() + "';")
    if cursor.rowcount != 0:
        user_actif['UserName'] = cursor.fetchone()[0]
        cursor.execute("select username from client where username = '" + username + "';")
        if cursor.rowcount != 0:
            user_actif['Profil'] = 'Acheteur'
        else:
            cursor.execute("select username from vendeur where username = '" + username + "';")
            if cursor.rowcount != 0:
                user_actif['Profil'] = 'Vendeur'
    db.close()
    if user_actif['UserName'] == '' and user_actif['Profil'] == '':
        return {}
    return user_actif

# Avec username, redonne un dictionnaire détaillé de l'utilisateur.
def infos_detail_user(user):
    db = MySQLdb.connect("localhost", "Equipe17", "SecurePassword", "projet_equipe17")
    cursor = db.cursor(cursorclass=MySQLdb.cursors.DictCursor)
    user_detail = {}
    if user['Profil'] == 'Vendeur':
        cursor.execute("select username, nom, courriel, telephone from utilisateur where username = '" + user['UserName'] + "';")
        user_detail = cursor.fetchone()
        user_detail['Profil'] = 'Vendeur'
        cursor.execute("SELECT balance FROM vendeur WHERE username = %s;", (user_detail['username'],))
        user_detail['balance'] = cursor.fetchone()['balance']
    if user['Profil'] == 'Acheteur':
        cursor.execute("select username, nom, courriel, telephone from utilisateur where username = '" + user['UserName'] + "';")
        user_detail = cursor.fetchone()
        user_detail['Profil'] = 'Acheteur'
        cursor.execute("SELECT naissance FROM client WHERE username = %s;", (user_detail['username'],))
        user_detail['naissance'] = cursor.fetchone()['naissance']
    db.close()
    return user_detail

# Retourne les articles d'une catégorie
def retourne_liste_articles(categorie):
    db = MySQLdb.connect("localhost", "Equipe17", "SecurePassword", "projet_equipe17")
    cursor = db.cursor()
    cursor.execute("select objet from appartient where categorie = '" + categorie + "';")
    liste_id = [item for sublist in cursor.fetchall() for item in sublist]
    liste = []
    for objet in liste_id:
        cursor.execute("select id, nom, prix, description, quantite from objet where id = '" + str(objet) + "';")
        article = cursor.fetchone()
        truc = {}
        truc['Id'] = article[0]
        truc['Nom'] = article[1]
        truc['Prix'] = article[2]
        truc['Description'] = article[3]
        truc['Quantite'] = article[4]
        cursor.execute("SELECT vendeur FROM offre WHERE objet = %s;", (truc['Id'],))
        username_vendeur = cursor.fetchone()
        vendeur = {}
        vendeur['username'] = username_vendeur[0]
        cursor.execute("SELECT nom FROM utilisateur WHERE username = %s;", (vendeur['username'],))
        vendeur_truc = cursor.fetchone()
        truc['Vendeur'] = vendeur_truc[0]
        liste.append(truc)
    db.close()
    return liste

# Retourne les infos sur un objet selon son id
def retourne_objet(id):
    db = MySQLdb.connect("localhost", "Equipe17", "SecurePassword", "projet_equipe17")
    cursor = db.cursor(cursorclass=MySQLdb.cursors.DictCursor)
    cursor.execute("select * from objet where id = " + id + ";")
    objet = cursor.fetchone()
    db.close()
    return objet

# Retourne la balance de l'utilisateur actif
def retourne_balance_actif(username):
    db = MySQLdb.connect("localhost", "Equipe17", "SecurePassword", "projet_equipe17")
    cursor = db.cursor()
    cursor.execute("select balance from vendeur where username = '" + username + "';")
    balance = cursor.fetchone()
    db.close()
    return balance

# Ajuste le montant de la balance du vendeur selon le montant donné
def ajuste_balance(username, montant):
    db = MySQLdb.connect("localhost", "Equipe17", "SecurePassword", "projet_equipe17")
    db.autocommit(True)
    cursor = db.cursor()
    cursor.execute("select balance from vendeur where username = '" + username + "';")
    retour = cursor.fetchone()
    balance = retour[0] + montant
    cursor.execute("update vendeur set balance = " + str(balance) + " where username = '" + username + "';")
    db.close()

def vendeur_from_object_id(id):
    db = MySQLdb.connect("localhost", "Equipe17", "SecurePassword", "projet_equipe17")
    cursor = db.cursor()
    query = "SELECT vendeur FROM offre WHERE objet = '" + id + "';"
    cursor.execute(query)
    vendeur = cursor.fetchone()
    return vendeur[0]

# Diminue la quantité disponible d'un objet vendu de 1 selon l'id
def diminue_quantite(id):
    db = MySQLdb.connect("localhost", "Equipe17", "SecurePassword", "projet_equipe17")
    db.autocommit(True)
    cursor = db.cursor()
    cursor.execute("select quantite from objet where id = " + id + ";")
    retour = cursor.fetchone()
    quantite = retour[0] - 1
    cursor.execute("update objet set quantite = " + str(quantite) + " where id = " + id + ";")
    db.close()

# Ajoute un produit dans la liste d'objets avec les descriptifs donnés
def ajoute_vente(id, nom, prix, quantite, description, categorie, image='None'):
    db = MySQLdb.connect("localhost", "Equipe17", "SecurePassword", "projet_equipe17")
    cursor = db.cursor()
    cursor.execute("insert into objet (id, nom, prix, quantite, description, image) values (" + id + ",'" +
               nom + "'," + prix + "," + quantite + ",'" + description + "','" + image + "');")
    cursor.execute("insert into appartient (objet, categorie) values (" + id + ",'" + categorie + "');")
    db.close()

# Ajout d'un nouvel acheteur
def ajoute_acheteur(username, password, nom, courriel, telephone, naissance):
    db = MySQLdb.connect("localhost", "Equipe17", "SecurePassword", "projet_equipe17")
    cursor = db.cursor()
    cursor.execute(
        "insert into utilisateur (username, password, nom, courriel, telephone) values ('"
        + username + "','" + hashlib.sha3_512(
            password.encode()).hexdigest() + "','" + nom + "','" + courriel + "','" + telephone + "');")
    cursor.execute("insert into client (username, naissance) values ('" + username + "','" + naissance + "');")
    db.close()

# Ajout d'un nouveau vendeur
def ajoute_vendeur(username, password, nom, courriel, telephone, num_compte='None', num_banque='NOT'):
    db = MySQLdb.connect("localhost", "Equipe17", "SecurePassword", "projet_equipe17")
    cursor = db.cursor()
    cursor.execute(
        "insert into utilisateur (username, password, nom, courriel, telephone) values ('"
        + username + "','" + hashlib.sha3_512(
            password.encode()).hexdigest() + "','" + nom + "','" + courriel + "','" + telephone + "');")
    cursor.execute(
        "insert into vendeur (username, num_compte_bancaire, num_banque) values ('" + username + "','"
        + num_compte + "','" + num_banque + "');")
    db.close()

# Retourne les catégories existantes
def retourne_categories():
    db = MySQLdb.connect("localhost", "Equipe17", "SecurePassword", "projet_equipe17")
    cursor = db.cursor()
    cursor.execute("select nom from categorie;")
    retour = cursor.fetchall()
    categories = [item for sublist in retour for item in sublist]
    db.close()
    print(retour)
    return categories