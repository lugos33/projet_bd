import MySQLdb
import hashlib
import random
from datetime import date
import mimesis
import mimesis.builtins

database = MySQLdb.connect(host='localhost', user='Equipe17', password='SecurePassword', charset='utf8')
database.autocommit(True)
cursor = database.cursor()


def add_client_bob():
    global cursor
    bob = {}
    bob['username'] = "Bob123"
    password_bob = "bobinette"
    bob['nom'] = "Bob Gratton"
    bob['courriel'] = "Bob.Gratton@email.com"
    bob['telephone'] = "324-888-7432"
    bob['hashed_password'] = hashlib.sha3_512(password_bob.encode()).hexdigest()

    query_bob = "INSERT INTO utilisateur (username, password, nom, courriel, telephone) VALUES (%s, %s, %s, %s, %s);"
    cursor.execute(query_bob, (bob['username'], bob['hashed_password'], bob['nom'], bob['courriel'], bob['telephone']))

    bob['date_naissance'] = "1975-07-09"

    query_bob = ("INSERT INTO client (username, naissance) VALUES (%s, %s);")
    cursor.execute(query_bob, (bob['username'], bob['date_naissance']))


def add_vendeur_garage():
    global cursor
    garage = {}
    garage['username'] = "GrosGarage"
    password = "YéGrosMonGarage"
    garage['hashed_password'] = hashlib.sha3_512(password.encode()).hexdigest()
    garage['nom'] = "Garage Chez Bob"
    garage['courriel'] = "OhCanada@chezbob.com"
    garage['telephone'] = "435-745-9384"
    garage['balance'] = "9765.31"
    garage['banque'] = "001"
    garage['num_compte'] = "7846654"

    query_garage = "INSERT INTO utilisateur (username, password, nom, courriel, telephone) VALUES (%s, %s, %s, %s, %s);"
    cursor.execute(query_garage, (garage['username'], garage['hashed_password'], garage['nom'], garage['courriel'],
                                  garage['telephone']))

    query_garage = ("INSERT INTO vendeur (username, balance, num_compte_bancaire, num_banque) VALUES (%s, %s, %s, %s);")
    cursor.execute(query_garage, (garage['username'], garage['balance'], garage['num_compte'], garage['banque']))


def add_clients():
    global cursor
    with open("./Populate data/password samples.txt") as passwords:
        with open("./Populate data/emails.txt") as emails:
            with open("./Populate data/noms.txt") as noms:
                with open("./Populate data/phones.txt") as phones:
                    with open("./Populate data/birthdate.txt") as naissances:
                        password_samples = passwords.read().splitlines()
                        noms_samples = noms.read().splitlines()
                        emails_samples = emails.read().splitlines()
                        phones_samples = phones.read().splitlines()
                        naissances_samples = naissances.read().splitlines()
                        for username in open("./Populate data/usernames client.txt").read().splitlines():
                            client = {}
                            client['username'] = username
                            password = random.choice(password_samples)
                            client['hashed_password'] = hashlib.sha3_512(password.encode()).hexdigest()
                            client['nom'] = random.choice(noms_samples)
                            client['courriel'] = random.choice(emails_samples)
                            client['phone'] = random.choice(phones_samples)
                            client['naissance'] = random.choice(naissances_samples)
                            query_client = """INSERT INTO utilisateur (username, password, nom, courriel, telephone) 
                            VALUES (%s, %s, %s, %s, %s);"""
                            cursor.execute(query_client, (client['username'], client['hashed_password'], client['nom'],
                                                          client['courriel'], client['phone']))
                            query_client = ("INSERT INTO client (username, naissance) VALUES (%s, %s);")
                            cursor.execute(query_client, (client['username'], client['naissance']))


def add_vendeurs():
    global cursor
    with open("./Populate data/password samples.txt") as passwords:
        with open("./Populate data/emails.txt") as emails:
            with open("./Populate data/companies.txt") as noms:
                with open("./Populate data/phones.txt") as phones:
                    with open("./Populate data/bank numbers.txt") as banks:
                        password_samples = passwords.read().splitlines()
                        noms_samples = noms.read().splitlines()
                        emails_samples = emails.read().splitlines()
                        phones_samples = phones.read().splitlines()
                        banks_samples = banks.read().splitlines()
                        for username in open("./Populate data/usernames vendor.txt").read().splitlines():
                            password = random.choice(password_samples)
                            vendeur = {}
                            vendeur['username'] = username
                            vendeur['hashed_password'] = hashlib.sha3_512(password.encode()).hexdigest()
                            vendeur['nom'] = random.choice(noms_samples)
                            vendeur['courriel'] = random.choice(emails_samples)
                            vendeur['phone'] = random.choice(phones_samples)
                            vendeur['bank'] = random.choice(banks_samples)
                            # Les numéros de comptes bancaires au Canada varient entre 7 et 12 chiffres
                            vendeur['account'] = str(random.randint(1000000, 999999999999))
                            vendeur['balance'] = str(round(random.uniform(0, 999999.99), 2))
                            query_vendeur = """INSERT INTO utilisateur (username, password, nom, courriel, telephone) 
                            VALUES ( %s, %s, %s, %s, %s);"""
                            cursor.execute(query_vendeur, (vendeur['username'], vendeur['hashed_password'],
                                                           vendeur['nom'], vendeur['courriel'], vendeur['phone']))
                            query_vendeur = """INSERT INTO vendeur (username, balance, num_compte_bancaire, num_banque) 
                            VALUES (%s, %s, %s, %s);"""
                            cursor.execute(query_vendeur, (vendeur['username'], vendeur['balance'], vendeur['account'],
                                                           vendeur['bank']))


def add_paiements():
    global cursor
    cursor.execute("SELECT username FROM vendeur;")
    vendeurs = [item for sublist in cursor.fetchall() for item in sublist]
    start_date = date.today().replace(day=1, month=1).toordinal()
    end_date = date.today().toordinal()
    for i in range(0, 500):
        paiement = {}
        paiement['vendeur'] = random.choice(vendeurs)
        paiement['num_confirmation'] = str(random.randint(1000000000, 999999999999999))
        paiement['montant'] = str(round(random.uniform(100, 49999.99), 2))
        paiement['date'] = str(date.fromordinal(random.randint(start_date, end_date)))
        query_paiement = """INSERT INTO paiement (montant, date_paiement, num_confirmation) VALUES (%s, %s, %s);"""
        cursor.execute(query_paiement, (paiement['montant'], paiement['date'], paiement['num_confirmation']))
        query_recoit = """INSERT INTO recoit (vendeur, paiement) VALUES (%s, LAST_INSERT_ID());"""
        cursor.execute(query_recoit, (paiement['vendeur'],))


def add_categories():
    global cursor
    for cat in open("./Populate data/categories.txt").read().splitlines():
        categorie = {}
        categorie['nom'] = cat
        query = """INSERT INTO categorie (nom) VALUES (%s);"""
        cursor.execute(query, (categorie['nom'],))


def add_objets():
    global cursor
    text_gen = mimesis.Text("en-ca")
    image_gen = mimesis.Internet("en-ca")
    noms = open("./Populate data/objects.txt").read().splitlines()
    cursor.execute("SELECT username FROM vendeur;")
    vendeurs = [item for sublist in cursor.fetchall() for item in sublist]
    cursor.execute("SELECT nom FROM categorie;")
    categories = [item for sublist in cursor.fetchall() for item in sublist]
    for i in range(1, 1500):
        item = {}
        item['nom'] = random.choice(noms)
        item['vendeur'] = random.choice(vendeurs)
        item['categorie'] = random.choice(categories)
        item['prix'] = str(round(random.uniform(1, 25000), 2))
        item['quantite'] = str(random.randint(1, 150))
        item['image'] = image_gen.image_placeholder()
        item['description'] = text_gen.text(5)
        query_objet = """BEGIN;
        INSERT INTO objet(prix, nom, image, quantite, description) VALUES (%s, %s, %s, %s, %s);
        SET @objid = LAST_INSERT_ID();
        INSERT INTO appartient (objet, categorie) VALUES (@objid, %s);
        INSERT INTO offre (vendeur, objet) VALUES (%s, @objid);
        COMMIT;"""
        cursor.execute(query_objet, (item['prix'], item['nom'], item['image'], item['quantite'], item['description'],
                                     item['categorie'], item['vendeur']))


def add_adresses():
    global cursor
    cursor.execute("SELECT username FROM client;")
    clients = [item for sublist in cursor.fetchall() for item in sublist]
    adress_gen = mimesis.Address("en-ca")
    for i in range(1, 500):
        adresse = {}
        adresse['num_civique'] = adress_gen.street_number(2500)
        adresse['rue'] = adress_gen.street_name() + " " + adress_gen.street_suffix()
        adresse['ville'] = adress_gen.city()
        adresse['province'] = adress_gen.province()
        adresse['code_postal'] = adress_gen.postal_code()
        adresse['client'] = random.choice(clients)
        query_adresse = """BEGIN;
        INSERT INTO adresse(num_civique, rue, ville, province, code_postal) VALUES (%s, %s, %s, %s, %s);
        INSERT INTO possede(client, adresse) VALUES (%s, LAST_INSERT_ID());
        COMMIT;"""
        cursor.execute(query_adresse, (adresse['num_civique'], adresse['rue'], adresse['ville'], adresse['province'],
                                       adresse['code_postal'], adresse['client']))


def add_livreurs():
    global cursor
    livreurs = open("./Populate data/livreurs.txt").read().splitlines()
    websites = open("./Populate data/websites_livreurs.txt").read().splitlines()
    for i in range(0, (len(livreurs))):
        livreur = {}
        livreur['nom'] = livreurs[i]
        livreur['website'] = websites[i]
        query_livreur = """INSERT INTO livreur(nom, site) VALUES (%s, %s);"""
        cursor.execute(query_livreur, (livreur['nom'], livreur['website']))


def add_transactions():
    global cursor
    start_date = date.today().replace(day=1, month=1).toordinal()
    tracking_number_gen = mimesis.builtins.USASpecProvider()
    end_date = date.today().toordinal()
    cursor.execute("SELECT client FROM possede;")
    clients = [item for sublist in cursor.fetchall() for item in sublist]
    cursor.execute("SELECT id FROM objet;")
    objets = [item for sublist in cursor.fetchall() for item in sublist]
    cursor.execute("SELECT nom FROM livreur;")
    livreurs = [item for sublist in cursor.fetchall() for item in sublist]
    for i in range(1, 1000):
        transaction = {}
        transaction['date'] = str(date.fromordinal(random.randint(start_date, end_date)))
        transaction['client'] = random.choice(clients)
        query_adresse = "SELECT adresse FROM possede WHERE client = %s;"
        cursor.execute(query_adresse, (transaction['client'],))
        adresses = [item for sublist in cursor.fetchall() for item in sublist]
        query_transaction = """BEGIN;
        INSERT INTO transaction(date_transaction) VALUES (%s);
        SET @transid = LAST_INSERT_ID();
        INSERT INTO fait(client, transaction) VALUES (%s, @transid);
        COMMIT;"""
        cursor.execute(query_transaction, (transaction['date'], transaction['client']))
        nb_livraisons = random.randint(1, 3)
        for liv in range(0, nb_livraisons):
            livraison = {}
            livraison['adresse'] = random.choice(adresses)
            livraison['suivi'] = tracking_number_gen.tracking_number()
            livraison['frais'] = str(round(random.uniform(5, 100), 2))
            livraison['livreur'] = random.choice(livreurs)
            query_livraison = """BEGIN;
            INSERT INTO livraison (num_suivi, frais) VALUES (%s, %s);
            SET @livid = LAST_INSERT_ID();
            INSERT INTO comprend(transaction, livraison) VALUES (@transid, @livid);
            INSERT INTO effectuee(livreur, livraison) VALUES (%s, @livid);
            INSERT INTO envoyee(livraison, adresse) VALUES (@livid, %s);
            COMMIT;"""
            cursor.execute(query_livraison, (livraison['suivi'], livraison['frais'], livraison['livreur'],
                                             livraison['adresse']))
            nb_objets = random.randint(1,5)
            objets_livres = random.sample(objets,nb_objets)
            for obj in objets_livres:
                objet = {}
                objet['id'] = obj
                query_prix = """SELECT prix FROM objet WHERE id = %s;"""
                cursor.execute(query_prix, (objet['id'],))
                liste_prix = [item for sublist in cursor.fetchall() for item in sublist]
                objet['prix'] = liste_prix[0]
                objet['quantite'] = str(random.randint(1, 5))
                query_objet = """INSERT INTO contient(objet, livraison, quantite, prix) VALUES (%s, @livid, %s, %s);"""
                cursor.execute(query_objet, (objet['id'], objet['quantite'], objet['prix']))


if __name__ == "__main__":

    # Supprime la database pour la recréer avec les versions plus avancées du script
    cursor.execute(("DROP DATABASE IF EXISTS projet_equipe17;"))

    # Création de la base de donnée
    cursor.execute("CREATE DATABASE projet_equipe17;")
    cursor.execute("USE projet_equipe17;")

    # Création des tables utilisateur, vendeur et client
    query = """CREATE TABLE utilisateur(username VARCHAR(15) NOT NULL, password VARCHAR(512) NOT NULL,
    nom VARCHAR(50) NOT NULL, courriel VARCHAR(50) NOT NULL, telephone VARCHAR(12), PRIMARY KEY(username));"""

    cursor.execute(query)

    query = """CREATE TABLE vendeur(username VARCHAR(15) NOT NULL, balance FLOAT(8,2) UNSIGNED NOT NULL 
    DEFAULT '0.00', num_compte_bancaire VARCHAR(12), num_banque CHAR(3), PRIMARY KEY(username), FOREIGN KEY(username) 
    REFERENCES utilisateur(username));"""

    cursor.execute(query)

    query = """CREATE TABLE client(username VARCHAR(15) NOT NULL, naissance DATE NOT NULL, 
    PRIMARY KEY(username), FOREIGN KEY(username) REFERENCES utilisateur(username));"""

    cursor.execute(query)

    # Insertion d'un compte prédéfini pour tester le côté des clients
    add_client_bob()
    # Insertion des clients
    add_clients()
    # Insertion d'un compte prédéfini pour tester le côté des vendeurs
    add_vendeur_garage()
    # Insertion des vendeurs
    add_vendeurs()

    # Création des autres tables

    query = """CREATE TABLE objet(id INT UNSIGNED NOT NULL AUTO_INCREMENT, prix FLOAT(7,2) UNSIGNED NOT NULL, nom 
    VARCHAR(250) NOT NULL, image VARCHAR(250), quantite SMALLINT UNSIGNED NOT NULL, description VARCHAR(750), PRIMARY
    KEY(id));"""
    cursor.execute(query)

    query = """CREATE TABLE paiement(id INT UNSIGNED NOT NULL AUTO_INCREMENT, montant FLOAT(8,2) UNSIGNED NOT NULL, 
    date_paiement DATE, num_confirmation VARCHAR(15), PRIMARY KEY (id));"""
    cursor.execute(query)

    query = """CREATE TABLE categorie(nom VARCHAR(60) NOT NULL, PRIMARY KEY(nom));"""
    cursor.execute(query)

    query = """CREATE TABLE adresse(id INT UNSIGNED NOT NULL AUTO_INCREMENT, num_civique INT UNSIGNED NOT NULL, rue 
    VARCHAR(50) NOT NULL, ville VARCHAR(60) NOT NULL, province VARCHAR(25) NOT NULL, code_postal VARCHAR(7) NOT NULL,
    PRIMARY KEY (id));"""
    cursor.execute(query)

    query = """CREATE TABLE livraison(id INT UNSIGNED NOT NULL AUTO_INCREMENT, num_suivi VARCHAR(25), frais FLOAT(5,2) 
    UNSIGNED NOT NULL, PRIMARY KEY (id));"""
    cursor.execute(query)

    query = """CREATE TABLE livreur(nom VARCHAR(50) NOT NULL, site VARCHAR(50), PRIMARY KEY (nom));"""
    cursor.execute(query)

    query = """CREATE TABLE transaction(id INT UNSIGNED NOT NULL AUTO_INCREMENT, date_transaction DATE NOT NULL,
    PRIMARY KEY (id));"""
    cursor.execute(query)

    query = """CREATE TABLE recoit(vendeur VARCHAR(15) NOT NULL, paiement INT UNSIGNED NOT NULL, PRIMARY KEY (vendeur, 
    paiement), FOREIGN KEY(vendeur) REFERENCES vendeur(username), FOREIGN KEY(paiement) REFERENCES paiement(id));"""
    cursor.execute(query)

    query = """CREATE TABLE effectuee(livreur VARCHAR(50) NOT NULL, livraison INT UNSIGNED NOT NULL, PRIMARY KEY 
     (livreur, livraison), FOREIGN KEY(livreur) REFERENCES livreur(nom), FOREIGN KEY(livraison) REFERENCES 
     livraison(id));"""
    cursor.execute(query)

    query = """CREATE TABLE envoyee(livraison INT UNSIGNED NOT NULL, adresse INT UNSIGNED NOT NULL, PRIMARY KEY
    (livraison, adresse), FOREIGN KEY(livraison) REFERENCES livraison(id), FOREIGN KEY(adresse) REFERENCES 
    adresse(id));"""
    cursor.execute(query)

    query = """CREATE TABLE possede(client VARCHAR(15) NOT NULL, adresse INT UNSIGNED NOT NULL, PRIMARY KEY
    (client, adresse), FOREIGN KEY(client) REFERENCES client(username), FOREIGN KEY(adresse) REFERENCES adresse(id));"""
    cursor.execute(query)

    query = """CREATE TABLE fait(client VARCHAR(15) NOT NULL, transaction INT UNSIGNED NOT NULL UNIQUE, PRIMARY
    KEY (client, transaction), FOREIGN KEY(client) REFERENCES client(username), FOREIGN KEY(transaction) REFERENCES
    transaction(id));"""
    cursor.execute(query)

    query = """CREATE TABLE offre(vendeur VARCHAR(15) NOT NULL, objet INT UNSIGNED NOT NULL UNIQUE, PRIMARY KEY
    (vendeur, objet), FOREIGN KEY(vendeur) REFERENCES vendeur(username), FOREIGN KEY(objet) REFERENCES objet(id));"""
    cursor.execute(query)

    query = """CREATE TABLE appartient(objet INT UNSIGNED NOT NULL, categorie VARCHAR(60) NOT NULL, PRIMARY KEY 
    (objet, categorie), FOREIGN KEY(objet) REFERENCES objet(id), FOREIGN KEY(categorie) REFERENCES categorie(nom));"""
    cursor.execute(query)

    query = """CREATE TABLE comprend(transaction INT UNSIGNED NOT NULL, livraison INT UNSIGNED NOT NULL UNIQUE, PRIMARY
    KEY (transaction, livraison), FOREIGN KEY(transaction) REFERENCES transaction(id), FOREIGN KEY(livraison) REFERENCES
    livraison(id));"""
    cursor.execute(query)

    query = """CREATE TABLE contient(objet INT UNSIGNED NOT NULL, livraison INT UNSIGNED NOT NULL, quantite INT UNSIGNED
    NOT NULL, prix FLOAT(7,2) UNSIGNED NOT NULL, PRIMARY KEY (objet, livraison), FOREIGN KEY(objet) REFERENCES 
    objet(id), FOREIGN KEY(livraison) REFERENCES livraison(id));"""
    cursor.execute(query)

    add_paiements()
    add_categories()
    add_objets()
    add_adresses()
    add_livreurs()
    add_transactions()

    # Création des triggers pour la date de naissance des clients
