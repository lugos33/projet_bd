# Projet de session GLO-2005 -- Équipe 17 #

La procédure ci-dessous assume que vous avez Python 3.6 et MySQL 5.7 d'installés sur votre machine. Si ce n'est pas le cas, procédez à l'installation avant de continuer.

## Installation des dépendances ##

Pour installer les dépendances du projet, exécutez la commande suivante en ligne de commande, à partir du répertoire du projet.

`pip install -r requirements.txt`

## Création de la base de données ##

Si vous devez vous créer une nouvelle base de données pour tester ou évaluer le projet, procédez de la manière suivante :

1. Créez l'utilisateur Equipe17 avec le mot de passe "SecurePassword" sur votre serveur MySQL local. En ligne de commande MySQL, il s'agit de la commande suivante.

`CREATE USER IF NOT EXISTS 'Equipe17'@'localhost' IDENTIFIED BY 'SecurePassword';` 

2. Donnez les privilèges nécessaires à l'utilisateur que vous avez créé avec la commande suivante dans MySQL. Le schéma projet_equipe17 sera celui utilisé par l'application. Il sera créé lorsque la base de données sera populée.

`GRANT ALL PRIVILEGES ON projet_equipe17.* TO 'Equipe17'@'localhost';`

3. Populez la base de données en exécutant la commande suivante en ligne de commande, à partir du répertoire du projet :

`python populate.py`

## Utilisation de l'application ##

Pour partir le serveur d'application Flask, exécutez la commande suivante en ligne de commande, à partir du répertoire du projet :

`python main.py`

Vous pouvez ensuite vous rendre à l'adresse 'localhost:5000' dans votre navigateur préféré pour accéder à l'application.

## Comptes de test ##

À des fins de tests, les comptes suivants sont assurés d'être créés lors de l'exécution du script:

- Compte client : Username: *Bob123*, Mot de passe: *bobinette*.
- Compte vendeur : Username: *GrosGarage*, Mot de passe: *YéGrosMonGarage*.

## Accès au code source ##

Le dépôt git du projet peut être consulté à l'adresse suivante : git@bitbucket.org:lugos33/projet_bd.git