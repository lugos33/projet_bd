from flask import Flask, render_template, request, redirect, url_for
import datetime
import acces_BD

app = Flask(__name__)

# Le nom d'utilisateur et son profil, qui ne sont pas des données confidentielles, sont présentes en tout temps
# dans le serveur d'application tant que l'utilisateur est connecté.
user_actif = {}
liste_users = []
liste_acheteurs = []
liste_vendeurs = []
liste_categorie = ['Aliments', 'Électronique', 'Cuisine', 'Meubles', 'Sport', 'Divers']
liste_objets = []
liste_users.append({'Prenom':'Luka', 'Nom':'Gosselin', 'UserName':'JarJar', 'MotDePasse':'Carotte', 'Date':'1995-12-12',
                    'Telephone':'4184961535', 'Courriel':'test123@hotmail.com'})
liste_acheteurs.append({'UserName':'JarJar', 'Balance':4000})
liste_users.append({'Prenom':'Georges', 'Nom':'Mac', 'UserName':'max2000', 'MotDePasse':'patate', 'Date':'1994-12-12',
                    'Telephone':'5145145454', 'Courriel':'cool23@gmail.com'})
liste_vendeurs.append({'UserName': 'max2000', 'Balance': 4000})
liste_objets.append({'Id':1, 'Nom':'Ballon de soccer', 'Quantite':5, 'Prix':12.50, 'Vendeur':'SportPro25',
                     'Description':'Ballon en très bonne condition. Un peu cher, mais à ne pas manquer.',
                     'Categorie':'Sport'})
liste_objets.append({'Id':2, 'Nom':'Pommes empire 5lb', 'Quantite':1, 'Prix':3.60, 'Vendeur':'FruitsVrac',
                     'Description':'Les meilleures pommes du monde!!!!!', 'Categorie':'Aliments'})
liste_objets.append({'Id':3, 'Nom':'Télévision 4K', 'Quantite':0, 'Prix':1024.85, 'Vendeur':'MrPatate100',
                     'Description':'Télé ordinaire.', 'Categorie':'Électronique'})
compteur = 4


# Page principale de l'application. Elle accueille l'utilisateur et lui présente ses options en fonction de
# son profil (déconnecté, acheteur, vendeur), qui est géré par le html correspondant.
@app.route("/")
def main():
    global user_actif
    return render_template('PagePrincipale.html', user=user_actif)


# Fonction servant à afficher la page pour qu'un vendeur mette en vente un produit. On vérifie d'abord que le
# client est connecté et qu'il est un vendeur, puis on lui affiche une page pour soumettre sa vente.
# On met comme variable la liste des catégories pour que l'utilisateur en prenne une qui existe.
@app.route('/vente')
def vente():
    if user_actif == {}:
        return render_template('Login.html', message='Veuillez vous connecter comme vendeur pour vendre svp')
    if user_actif['Profil'] == 'Acheteur':
        return render_template('Login.html', message='Veuillez vous connecter comme vendeur pour vendre svp')
    return render_template('Vente.html', categories=acces_BD.retourne_categories())


# Fonction qui valide ses paramètres et créée une vente pour être affichée aux autres utilisateurs
@app.route('/vente', methods=['POST'])
def confirme_vente():
    nom = request.form.get('nom')
    categorie = request.form.get('categorie')
    prix = request.form.get('prix')
    quantite = request.form.get('quantite')
    description = request.form.get('description')
    if nom == '' or prix == '' or quantite == '' or description == '':
        return render_template('Vente.html', user=user_actif, categories=liste_categorie,
                               message='Un ou plusieurs paramètres sont manquants!')
    prix = float(prix)
    quantite = int(quantite)
    acces_BD.ajoute_vente(nom, prix, quantite, description, categorie)
    return render_template('ConfirmationVente.html')


# Fonction qui ne fait qu'afficher la page pour créer un compte
@app.route('/signup')
def creation_compte():
    return render_template('SignUp.html')


# Affiche la page de connection. Si l'utilisateur est déjà connecté, on lui affiche son nom d'utilisateur et
# son profil
@app.route("/login")
def premier_login():
    return render_template("Login.html", utilisateur=user_actif)


# Essaye de connecter l'utilisateur par la demande venant du html de la page de connection. Elle vérifie si les
# champs sont valides, puis tente de trouver un utilisateur correspondant et de le connecter. Si ça fonctionne,
# on retourne à la même page en accueillant l'utilisateur. Sinon, on affiche un message d'erreur.
@app.route("/login", methods=['POST'])
def confirme_login():
    global user_actif
    username = request.form.get('username')
    motpasse = request.form.get('motpasse')
    if not username or not motpasse:
        return render_template('Login.html', message='Veuillez remplir tous les champs svp!')
    elif not valide_username(username):
        return render_template('Login.html', message='Profil introuvable!')

        # Vérifier username et mot de passe avec DB.
    user_actif = acces_BD.info_user(username, motpasse)
    if user_actif != {}:
        return render_template('Login.html', utilisateur=user_actif)
    return render_template('Login.html', message='Profil introuvable!')


# Efface l'utilisateur de user_actif et confirme la manoeuvre en affichant une page de confirmation.
@app.route('/logout')
def deconnection():
    global user_actif
    user_actif = {}
    return render_template('LogOut.html')


# Valide les informations reçues et créé un compte selon ce qui a été remplit, si le compte n'existe pas déjà présent.
@app.route('/signup', methods=['POST'])
def sign_up():
    fail = False
    nom = request.form.get('nom')
    username = request.form.get('username')
    motpasse = request.form.get('motpasse')
    date_naissance = request.form.get('date')
    telephone = request.form.get('telephone')
    if telephone == '':
        telephone = 'N/A'
    courriel = request.form.get('courriel')
    adresse = {}
    adresse['Ville'] = request.form.get('ville')
    adresse['Province'] = request.form.get('province')
    adresse['CodePostal'] = request.form.get('codepostal')
    adresse['NumeroCivique'] = request.form.get('numero_civique')
    adresse['Rue'] = request.form.get('rue')
    if compte_existe_deja(username):
        return render_template('SignUp.html', message='Ce compte existe déjà.')
    if not nom or not username or not motpasse or not date_naissance or not courriel\
            or not adresse['Ville'] or not adresse['CodePostal'] or not adresse['NumeroCivique'] or not adresse['Rue']\
            or not request.form.get('type'):
        fail = True
    if not valide_nom(nom) or not valide_username(username):
        fail = True
    if motpasse != request.form.get('motpasse2'):
        return render_template('SignUp.html', message='Le mot de passe n\'est pas le même dans les deux champs')
    if adresse['NumeroCivique'] != '':
        if int(adresse['NumeroCivique']) < 0:
            return render_template('SignUp.html', message='Le numéro de l\'adresse est invalide')
    if date_naissance != '':
        if valide_age(date_naissance) < 18 or valide_age(date_naissance) == False:
            return render_template('SignUp.html', message='Il faut être âgé d\'au moins 18 ans pour s\'inscrire')
    if fail:
        return render_template('SignUp.html', message='La création du compte a échoué, car une ou plusieurs'
                                                      ' informations sont invalides ou manquantes')
    adresse['NumeroCivique'] = int(adresse['NumeroCivique'])
    user={'Nom':nom, 'UserName':username, 'MotDePasse':motpasse,
                        'Date':date_naissance, 'Telephone':telephone, 'Courriel':courriel, 'Adresse':adresse}
    type = request.form.get('type')
    profil = {'UserName':username}
    if type == 'Vendeur':
        profil['Balance'] = 0
    ajoute_user(user, profil, type)
    user_actif['UserName'] = username
    user_actif['Profil'] = type
    return render_template('ConfirmeCompte.html', utilisateur=user_actif)


# Affiche les informations de l'utilisateur détaillées, seulement s'il est connecté.
@app.route('/compte')
def infos_compte():
    if len(user_actif) != 0:
        '''if user_actif['Profil'] == 'Vendeur':
            balance = acces_BD.retourne_balance_actif(user_actif['UserName'])
            return render_template('Compte.html', utilisateur=acces_BD.infos_detail_user(user_actif),
                                   balance=balance)'''
        return render_template('Compte.html', utilisateur=acces_BD.infos_detail_user(user_actif))
    return render_template('Login.html', utilisateur=user_actif)


# Modifie la balance de l'utilisateur selon le montant qui a été donné.
@app.route('/balance', methods=['POST'])
def modification_balance():
    if user_actif != {}:
        montant = float(request.form.get('prix'))
        balance = acces_BD.retourne_balance_actif()
        if (balance + montant) < 0:
            return render_template('Balance.html', accepte=False)
        acces_BD.ajuste_balance(montant)
        return render_template('Balance.html', montant=montant, balance=balance, accepte=True)
    return render_template('Login.html')


# Affiche les catégories que l'utilisateur peut sélectionner, selon ce qui est présent en mémoire
@app.route('/categories')
def categories():
    return render_template('Categories.html', categories=acces_BD.retourne_categories())


# Lorsqu'une catégorie est spécifiée, on affiche les articles appartenant à cette catégorie
@app.route('/categories/<categorie>')
def categories_ou_objets(categorie):
    objets_voulus = acces_BD.retourne_liste_articles(categorie)
    if len(objets_voulus) == 0:
        return render_template('Objet.html', categorie=categorie)
    return render_template('Objet.html', categorie=categorie, objets=objets_voulus)


# Affiche une page personnalisée de l'objet selon son id. Si l'objet est introuvalble, on affiche une erreur.
# On présente l'option d'acheter si l'utilisateur est un acheteur.
@app.route('/objet/<id>')
def page_objet(id):
    print(user_actif)
    objet = acces_BD.retourne_objet(id)
    if objet != {}:
        return render_template('PageObjet.html', objet=objet, user=user_actif)
    return render_template('PageObjet.html')


# Affiche les informations sur l'objet que l'utilisateur s'apprête à acheter, en confirmant que c'est bien un acheteur.
# On affiche son solde actuel et ce qu'il restera après. S'il ne reste plus de cet article ou que le solde du client
# est trop bas, il ne peut faire l'achat.
@app.route('/confirmeachat/<id>')
def confirme_achat(id):
    id = id
    if user_actif == {}:
        return render_template('Login.html', utilisateur=user_actif)
    elif user_actif['Profil'] == 'Vendeur':
        return render_template('Login.html', utilisateur=user_actif)
    objet = acces_BD.retourne_objet(id)
    if objet == {}:
        render_template('ConfirmeAchat.html')
    elif objet['quantite'] == 0:
        return render_template('ConfirmeAchat.html', disponible=False)
    return render_template('ConfirmeAchat.html', disponible=True, username=user_actif['UserName'], objet=objet)


# Complète l'achat en ajoutant l'argent au vendeur, en enlevant l'argent à l'acheteur et en diminuant de 1 la
# quantité de l'article.
@app.route('/complet/<id>', methods=['POST'])
def achat_confirme(id):
    if user_actif == {}:
        return render_template('AchatIncomplet.html', message='Aucun compte actif.')
    objet = acces_BD.retourne_objet(id)
    if objet['quantite'] == 0:
        return render_template('AchatIncomplet.html', message='Objet indisponible.')
    vendeur = acces_BD.vendeur_from_object_id(id)
    acces_BD.ajuste_balance(vendeur, objet['prix'])
    acces_BD.diminue_quantite(id)
    return render_template('Confirmation.html')

# Valide le username
def valide_username(username):
    if username == '':
        return False
    for lettre in username:
        if not (lettre.isalnum() or lettre in ['-', '_'] or lettre.isspace()):
            return False
    return True


# Valide l'age et le calcule
def valide_age(date):
    annee = date[: 4]
    mois = date[5: 7]
    jour = date[8:]
    if annee == 'aaaa' or mois == 'mm' or jour == 'jj':
        return False
    today = datetime.date.today()
    naissance = datetime.date(int(annee), int(mois), int(jour))
    return today.year - naissance.year - ((today.month, today.day) < (naissance.month, naissance.day))


# Valide un nom
def valide_nom(nom):
    if nom == '':
        return False
    for lettre in nom:
        if not (lettre.isalpha() or lettre.isspace()):
            return False
    return True

if __name__ == "__main__":
    app.run()
